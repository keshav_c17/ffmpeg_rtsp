#!/bin/bash

mkdir -p ~/ffmpeg_build && \

cd ~/ffmpeg_build && \

wget -O ffmpeg-6.1.tar.bz2 https://ffmpeg.org/releases/ffmpeg-6.1.tar.bz2 && \

tar -xjvf ffmpeg-6.1.tar.bz2 && \

cd ffmpeg-6.1 && \
  ./configure \
  --extra-cxxflags="-march=native" \
  --extra-libs="-lpthread -lm" \
  --ld="g++" \
  --enable-shared \
  --enable-gpl \
  --enable-pthreads \
  --enable-gnutls \
  --enable-libass \
  --enable-libfreetype \
  --enable-libmp3lame \
  --enable-libvpx \
  --enable-libx264 \
  --enable-libx265 && \

make -j $(( $(nproc) - 2 )) && \

sudo make install && \

sudo ldconfig
